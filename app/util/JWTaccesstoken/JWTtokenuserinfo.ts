import { Request, Response, NextFunction, request } from 'express'
import { authentication, AuthenticateModel } from '../../models/AuthenticationModel'
import { Constant } from '../constant/Constant'
import { ErrorHandle } from '../error/Error';

export class JWTTokenUserInfo {

    userInfo(req: any, res: Response, next: NextFunction) {
        const userid = req.token
        AuthenticateModel.findById(userid).select('_id isVerified isDeleted name mobileNumber email password')
            .then((result: authentication | null) => {
                if (result?.isDeleted == true || result?.isVerified == false) {
                    throw Constant.INVALID_USER
                }
                req.userInfo = result
                next()
            }).catch((err: any) => {
                if (err == Constant.INVALID_USER)
                    res.status(Constant.STATUS_OK).json({ status: Constant.FAILURE, message: Constant.INVALID_USER })
                else if (!err.statuscode) {
                    err.statuscode = Constant.CANCEL;
                }
                else
                    next(err)
            })
    }

    // role(role: Array<string>): any {
    //     return (req: Request, res: Response, next: NextFunction) => {
    //         if (role.indexOf(req.body.tokenUser.role) > -1) {
    //             next()
    //         } else {
    //             res.status(Constant.STATUS_OK).json({ status: Constant.FAILURE, message: Constant.ROLE_ERROR })
    //         }
    //     }
    // }
}