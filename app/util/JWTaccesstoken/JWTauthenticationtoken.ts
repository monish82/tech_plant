import { Request, Response, NextFunction, request } from 'express'
import * as jwt from "jsonwebtoken"
import { Constant } from '../constant/Constant'

export class JWTTokenAuthenticate {

    validate(req: any, res: Response, next: NextFunction) {
        const authHeader = req.get("Authorization")
        if (!authHeader) {
            const error = new Error('Not authenticated.')
            throw error
        }
        const token = authHeader.split(' ')[1]
        let decodedToken: any;
        try {
            decodedToken = jwt.verify(token, Constant.AGENCY_KEY);
        } catch (err) {
            err.statusCode = 500;
            throw err;
        }
        req.token = decodedToken.id
        next()

    }
}