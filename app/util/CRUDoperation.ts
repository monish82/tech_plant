import { Response, Request, NextFunction } from 'express'

export interface CURDOperation {
    create(req: Request, res: Response, next: NextFunction): any
    get(req: Request, res: Response, next: NextFunction): any
    put(req: Request, res: Response, next: NextFunction): any
    delete(req: Request, res: Response, next: NextFunction): any
}

