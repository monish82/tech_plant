import multer from 'multer'
import multerS3 from 'multer-s3'
import * as aws from 'aws-sdk'

import { Constant } from '../constant/Constant';

aws.config.update({
    secretAccessKey: process.env.secretAccessKey,
    accessKeyId: process.env.accessKeyId,
    region: 'ap-south-1'
})

const s3 = new aws.S3()
export const multerStorage = (storageFolder: string) => {
    return multer({
        storage: multerS3({
            s3: s3,
            bucket: 'plant-images',//miziontri-root  cabs-upload-images
            key: function (req: any, file: any, cb: any) {
                var newFileName = Date.now() + "-" + file.originalname;
                // var fullPath = newFileName;
                cb(null, newFileName)
            }
        }),
        fileFilter: imageFileFilter
    })
};

export const imageFileFilter = (req: any, file: any, cb: any) => {
    if (
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/*'
    ) {
        return cb(null, true);
    } else {
        return cb(null, false);
    }
};