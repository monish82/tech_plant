const sendOtp = require('sendotp')

export class Constant {


  public static MOBILENUMBER_IS_ALREDY_REGISTERD: string = "Mobilenumber is already registerd"
  public static STATUS_OK: number = 200
  public static CONFLICT: number = 409
  public static CANCEL: number = 500
  public static REGISTER_SUCCESFULLY: string = "Registered successfully"
  public static SOMETHING_WENT_WRONG: string = "Something went Wrong"
  public static YOUR_MOBILENUMBER_IS_NOT_REGISTERED: string = "Mobilenumber is Not Registered"
  public static PASSSWORD_IS_INVALID: string = "Password is Invalid"
  public static LOGIN_SUCCESSFULLY: string = "Login Successfully"
  public static AGENCY_KEY = `${process.env.KEY}`
  public static INVALID_USER: any = "Invalid User"
  public static ROLE_ERROR: any = ""
  public static FAILURE: any = 0
  public static SUCCESS: any = 1
  public static MSG_91: any = `${process.env.MSG_91_KEY}`
  public static MSG_91_SUCCESS: any = "success"
  public static OTP_SENT_SUCCESSFULLY_TO_YOUR_REGISTER_MOBILENUMBER: any = "OTP Sent Successfully To Your Register Mobile Number"
  public static ERROR: any = "error"
  public static PLANT_SAVED_SUCCESFULLY: any = "Plant Save Successfully"
  public static INAVLID_OTP: any = "Invalid Otp"
  public static OTP_VERIFIED_SUCCESSFULLY: any = "Otp Verified Successfully"

  public static async logConsole(key: string, message: any) {
    if (process.env.NODE_ENV)
      console.log(key, message)
  }

  // public static sendOTP(mobile: string): Promise<boolean> {
  //   console.log(mobile)
  //   const sendotp = new sendOtp(Constant.MSG_91, 'Use OTP to join us{{otp}}. \n Tech team')
  //   return new Promise((resolve, reject) => {
  //     sendotp.send(`91 `, "Plantae", (error: any, data: any) => {
  //       console.log(data)
  //       if (data.type == Constant.MSG_91_SUCCESS)
  //         return resolve(true)
  //       else
  //         return resolve(false)
  //     })
  //   })
  // }

  public static sendOTP(mobile: string): Promise<boolean> {
    const sendotp = new sendOtp(Constant.MSG_91, 'Use OTP to join us{{otp}}. \n tech team')
    return new Promise((resolve, reject) => {
      sendotp.send(`91${mobile}`, "MIZION", (error: any, data: any) => {
        if (data.type == Constant.MSG_91_SUCCESS)
          return resolve(true)
        else
          return resolve(false)
      })
    })
  }

  public static verifyOTP(mobileNumber: String, otp: String): Promise<boolean> {
    const sendotp = new sendOtp(Constant.MSG_91, 'Please enter OTP and verify the mobile {{otp}}')
    return new Promise((resolve: any, reject: any) => {
      sendotp.verify(`91${mobileNumber}`, otp, (error: any, data: any) => {
        if (data.type == Constant.MSG_91_SUCCESS) {
          return resolve(true)
        } else if (data.type == Constant.ERROR) {
          return resolve(false)
        } else {
          return reject(Constant.SOMETHING_WENT_WRONG)
        }
      })
    })
  }

}

