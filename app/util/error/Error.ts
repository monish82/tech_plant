import { Request, Response, NextFunction, ErrorRequestHandler } from 'express';
import { Error } from 'mongoose';
export class ErrorHandle {
    public get404(req: Request, res: Response, next: NextFunction) {
        res.status(404).json({ message: 'We are working around the issue 404' });
    }

    public get500(err: ErrorRequestHandler, next: NextFunction) {
        const errorObj: any = new Error('Invalid user')
        errorObj.statusCode = 500//constantValues.statusException
        return next(errorObj);
    }
}