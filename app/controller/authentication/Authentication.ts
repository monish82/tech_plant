import { Response, Request, NextFunction } from 'express'
import { AuthenticateModel, authentication } from '../../models/AuthenticationModel';
import { Constant } from '../../util/constant/Constant';
import { AuthenticationHandler } from "./AuthenticationHandler";
import * as bcrypt from 'bcryptjs';
import * as jwt from "jsonwebtoken"
import auth from 'basic-auth';

export class Authentication implements AuthenticationHandler {

  create(req: any, res: Response, next: NextFunction) {

  }

  get(req: Request, res: Response, next: NextFunction) {

  }

  put(req: Request, res: Response, next: NextFunction) {

  }

  delete(req: Request, res: Response, next: NextFunction) {

  }

  signup(req: Request, res: Response, next: NextFunction) {
    const body = req.body
    const name = body.name
    const mobileNumber: any = body.mobileNumber
    const email = body.email
    const password = body.password
    let salt = bcrypt.genSaltSync(10)
    let hash = bcrypt.hashSync(password, salt)

    AuthenticateModel.findOne({ mobileNumber: mobileNumber })
      .then((result: any) => {
        if (result) {
          throw Constant.MOBILENUMBER_IS_ALREDY_REGISTERD
        } else {
          return AuthenticateModel.create({
            name: name,
            mobileNumber: mobileNumber,
            email: email,
            password: hash
          })
        }
      })
      .then((result: any) => {
        if (result)
          return Constant.sendOTP(mobileNumber)
        else
          res.status(Constant.STATUS_OK).json({ status: 0, message: Constant.SOMETHING_WENT_WRONG })
      }).then((result: any) => {
        console.log(result)
        if (result)
          res.status(Constant.STATUS_OK).json({ status: 1, message: Constant.OTP_SENT_SUCCESSFULLY_TO_YOUR_REGISTER_MOBILENUMBER })
        else
          res.status(Constant.STATUS_OK).json({ status: 0, message: Constant.SOMETHING_WENT_WRONG })
      }).catch((err: any) => {
        Constant.logConsole("error===", err)
        if (err == Constant.MOBILENUMBER_IS_ALREDY_REGISTERD) {
          res.status(Constant.STATUS_OK).json({ status: 0, message: Constant.MOBILENUMBER_IS_ALREDY_REGISTERD })
        }
        else if (!err.statuscode) {
          err.statuscode = Constant.CANCEL;
        }
        else
          next(err)
      })
  }

  verifyOtp(req: Request, res: Response, next: NextFunction) {
    const mobileNumber: any = req.body.mobileNumber
    const otp = req.body.otp

    Constant.verifyOTP(mobileNumber, otp)
      .then((result: any) => {
        if (result)
          return AuthenticateModel.findOne({ mobileNumber: mobileNumber })
        else
          throw Constant.INAVLID_OTP
      }).then((result: any) => {
        if (result)
          return AuthenticateModel.updateOne(result, { $set: { isVerified: true } })
        else
          throw Constant.SOMETHING_WENT_WRONG
      }).then((result: any) => {
        if (result)
          res.status(Constant.STATUS_OK).json({ status: 1, message: Constant.OTP_VERIFIED_SUCCESSFULLY })
        else
          res.status(Constant.STATUS_OK).json({ status: 0, message: Constant.SOMETHING_WENT_WRONG })
      }).catch((err: any) => {
        if (err == Constant.INAVLID_OTP)
          res.status(Constant.STATUS_OK).json({ status: 0, message: Constant.INAVLID_OTP })
        else if (err == Constant.SOMETHING_WENT_WRONG)
          res.status(Constant.STATUS_OK).json({ status: 0, message: Constant.SOMETHING_WENT_WRONG })
        else if (!err.statuscode) {
          err.statuscode = Constant.CANCEL;
        }
        else
          next(err)
      })

  }

  login(req: Request, res: Response, next: NextFunction) {
    const credentials = auth(req)
    const mobileNumber = credentials!.name
    const password = credentials!.pass


    AuthenticateModel.findOne({ mobileNumber: mobileNumber })
      .then((result: any) => {
        if (result) {
          bcrypt.compare(password, result.password)
            .then((domatch) => {
              if (domatch) {
                const token = jwt.sign({
                  id: result._id
                }, Constant.AGENCY_KEY)
                res.status(Constant.STATUS_OK).json({ status: 1, message: Constant.LOGIN_SUCCESSFULLY, token: token })
              }
              else
                res.status(Constant.STATUS_OK).json({ status: 0, message: Constant.PASSSWORD_IS_INVALID })
            })
        } else
          throw Constant.YOUR_MOBILENUMBER_IS_NOT_REGISTERED
      }).catch((err: any) => {
        if (err == Constant.YOUR_MOBILENUMBER_IS_NOT_REGISTERED)
          res.status(Constant.STATUS_OK).json({ status: 0, message: Constant.YOUR_MOBILENUMBER_IS_NOT_REGISTERED })
        else if (!err.statuscode)
          err.statuscode = Constant.CANCEL;
        else
          next(err)
      })
  }


}