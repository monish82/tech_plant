import { CURDOperation } from '../../util/CRUDoperation'
import { Response, Request, NextFunction } from 'express'

export interface AuthenticationHandler extends CURDOperation {

    signup(req: Request, res: Response, next: NextFunction): any
    login(req: Request, res: Response, next: NextFunction): any
}