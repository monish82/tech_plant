import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import { Authentication } from '../controller/authentication/Authentication';
import { JWTTokenAuthenticate } from '../util/JWTaccesstoken/JWTauthenticationtoken';
import { JWTTokenUserInfo } from '../util/JWTaccesstoken/JWTtokenuserinfo';


export class AuthenticationRouter {

    router(): express.Router {
        const router = express.Router()

        router.post('/create', Authentication.prototype.signup)
        router.put('/verify', Authentication.prototype.verifyOtp)
        router.post('/login', Authentication.prototype.login)
        
        return router
    }
}