import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import { AuthenticationRouter } from './authentication';
import { UsersRouter } from './users';



export class Router {

    public router(): express.Router {
        let app = express.Router()

        app.use('/authentication', AuthenticationRouter.prototype.router())

        app.use('/users', UsersRouter.prototype.router())

        return app;
    }
}