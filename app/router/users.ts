import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import { Plant } from '../controller/plant/Plant';
import { JWTTokenAuthenticate } from '../util/JWTaccesstoken/JWTauthenticationtoken';
import { JWTTokenUserInfo } from '../util/JWTaccesstoken/JWTtokenuserinfo';
import * as multerFile from '../util/multer/multer'
const fileImageUpload = multerFile.multerStorage('images')


export class UsersRouter {

    router(): express.Router {
        const router = express.Router()

        const cpUpload = fileImageUpload.fields([{ name: 'plantImages', maxCount: 10 }
        ])

        router.post('/createplant', [JWTTokenAuthenticate.prototype.validate, JWTTokenUserInfo.prototype.userInfo], cpUpload, Plant.prototype.create)


        return router
    }
}