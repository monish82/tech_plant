import app from './app';
import * as http from 'http';
const Port = 9498;

http.createServer(app).listen(process.env.Port || Port, () => {
    console.log("express server is listen", Port)
})