import { prop, Typegoose, ModelType, InstanceType, Ref, index, arrayProp } from 'typegoose';

@index({ mobileNumber: 1 }, { unique: true })
export class plantClassification extends Typegoose {
    @prop({ required: true })
    public gene!: string

    public getGene(): string {
        return this.gene;
    }

    public setGene(gene: string): void {
        this.gene = gene;
    }


    @prop({ required: false })
    public species!: string

    public getSpecies(): string {
        return this.species;
    }

    public setSpecies(species: string): void {
        this.species = species;
    }


    @prop({ required: false })
    public family!: string

    public getFamily(): string {
        return this.family;
    }

    public setFamily(family: string): void {
        this.family = family;
    }


    @prop({ required: true })
    public division!: string

    public getDivision(): string {
        return this.division;
    }

    public setDivision(division: string): void {
        this.division = division;
    }

}


export class plantUses extends Typegoose {
    @prop({ required: true })
    public medicalUses!: string

    public getMedicalUses(): string {
        return this.medicalUses;
    }

    public setMedicalUses(medicalUses: string): void {
        this.medicalUses = medicalUses;
    }


    @prop({ required: true })
    public generalUses!: string

    public getGeneralUses(): string {
        return this.generalUses;
    }

    public setGeneralUses(generalUses: string): void {
        this.generalUses = generalUses;
    }

}

export class plantImages extends Typegoose {
    @prop({ required: true, maxlength: 400 })
    public filePath!: string


    @prop({ required: true, maxlength: 30 })
    public mimetype!: string


    @prop({ required: true, maxlength: 50 })
    public originalname!: string

    @prop({ required: true })
    public size!: number

}


export class plantDetails extends Typegoose {

    @prop({ required: true, trim: true, minlength: 3, maxlength: 30 })
    public plantName!: string;

    public getPlantName(): string {
        return this.plantName;
    }

    public setPlantName(plantName: string): void {
        this.plantName = plantName;
    }


    @prop({ required: true, trim: true, minlength: 8, maxlength: 60 })
    public plantDescription!: string;

    public getPlantDescription(): string {
        return this.plantDescription;
    }

    public setPlantDescription(plantDescription: string): void {
        this.plantDescription = plantDescription;
    }


    @prop({ required: true, trim: true, minlength: 8, maxlength: 60 })
    public plantQRcode!: string;

    public getPlantQRcode(): string {
        return this.plantQRcode;
    }

    public setPlantQRcode(plantQRcode: string): void {
        this.plantQRcode = plantQRcode;
    }


    @arrayProp({ items: plantImages })
    public plantImages!: plantImages[]

    public getPlantImages(): plantImages[] {
        return this.plantImages;
    }

    public setPlantImages(plantImages: plantImages[]): void {
        this.plantImages = plantImages;
    }


    @prop({ _id: false })
    public plantClassification!: plantClassification

    public getPlantClassification(): plantClassification {
        return this.plantClassification;
    }

    public setPlantClassification(plantClassification: plantClassification): void {
        this.plantClassification = plantClassification;
    }


    @prop({ _id: false })
    public plantUses!: plantUses

    public getPlantUses(): plantUses {
        return this.plantUses;
    }

    public setPlantUses(plantUses: plantUses): void {
        this.plantUses = plantUses;
    }


    @prop({ required: true })
    public plantAdvantage!: string

    public getPlantAdvantage(): string {
        return this.plantAdvantage;
    }

    public setPlantAdvantage(plantAdvantage: string): void {
        this.plantAdvantage = plantAdvantage;
    }


    @prop({ required: true })
    public plantDisadvantange!: string

    public getPlantDisadvantange(): string {
        return this.plantDisadvantange;
    }

    public setPlantDisadvantange(plantDisadvantange: string): void {
        this.plantDisadvantange = plantDisadvantange;
    }


    @prop({ required: true, default: Date.now })
    public createAt!: Date

    public getCreateAt(): Date {
        return this.createAt;
    }

    public setCreateAt(createAt: Date): void {
        this.createAt = createAt;
    }


    @prop({ required: true, default: Date.now })
    public modifiedAt!: Date

    public getModifiedAt(): Date {
        return this.modifiedAt;
    }

    public setModifiedAt(modifiedAt: Date): void {
        this.modifiedAt = modifiedAt;
    }


    @prop({ required: true, default: false })
    public isDeleted!: boolean

    public isIsDeleted(): boolean {
        return this.isDeleted;
    }

    public setIsDeleted(isDeleted: boolean): void {
        this.isDeleted = isDeleted;
    }

}

export const PlantModel = new plantDetails().getModelForClass(plantDetails)