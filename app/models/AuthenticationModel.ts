import { prop, Typegoose, ModelType, InstanceType, Ref, index } from 'typegoose';

@index({ mobileNumber: 1 }, { unique: true })
export class authentication extends Typegoose {

    @prop({ required: true, trim: true, minlength: 3, maxlength: 30, match: /[a-f]*/ })
    public name!: string;

    @prop({ required: true, trim: true, minlength: 10, maxlength: 10, match: /[0-9]*/ })
    public mobileNumber!: string;

    @prop({ required: false })
    public email?: string;

    @prop({ required: true, trim: true })
    public password!: string;

    @prop({ required: true, default: Date.now })
    public createAt!: Date

    @prop({ required: true, default: false })
    public isVerified!: boolean

    @prop({ required: true, default: Date.now })
    public modifiedAt!: Date

    @prop({ required: true, default: false })
    public isDeleted!: boolean
}

export const AuthenticateModel = new authentication().getModelForClass(authentication)