import * as express from 'express';
import { Request, Response, NextFunction, ErrorRequestHandler } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { Router } from './router/router';
import { Constant } from './util/constant/Constant';
const mongoose = require('mongoose');


class App {
    public app: express.Application = express.default()
    private router: Router = new Router()

    constructor() {
        this.config()
        this.mongoSetup()
        this.app.use('/api', this.router.router())
        this.app.use((error: ErrorRequestHandler, req: Request, res: Response, next: NextFunction) => {
            res.status(Constant.STATUS_OK).json({ success: 0, message: `${error}` })
        })
    }

    config() {
        this.app.use(cors())
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded({ extended: false }))
    }

    mongoSetup() {
        let mongoUrl: any = ""
        if (process.env.NODE_ENV === 'development')
            mongoUrl = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.vngyu.mongodb.net/${process.env.MONGO_DATABASE}?retryWrites=true&w=majority`;

        console.log('mongourl', process.env.MONGO_USER)
        mongoose.Promise = global.Promise;
        mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true }, (err: any, result: any) => {
            if (!err) {
                console.log('connected to database!')
            } else {
                console.log(err)
            }
        })
    }
}

export default new App().app
